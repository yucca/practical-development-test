# Bray Leino Yucca – Practical Development Test

The purpose of this test is to provide us with an example of your web development style and ability.

The objective is for you to create a simple ASP.NET Web Application. The application will read records from a data source then display them on a webpage. The user will have the option to add new records and update existing records.

### Data source
-	Source: ‘Dataset.json’.
-	The data source is a file that contains a list of real estate properties formatted in JSON.

### Part One
-	Create a webpage to list all of the records.
-	The user should be able to sort the records by the ‘sale_price’ and ‘_timestmap’ fields.
-	A clean and simple design should be incorporated.

### Part Two
-	The user should be able add new records and update existing records.
-	New records and updates should be saved to the JSON file.

### Part Three
-	What technical decsions did you make and why?
-	If you had more time what could be done to improve on your solution?

*Your solution should be provided as a Visual Studio project (.sln).*